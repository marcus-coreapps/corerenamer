/*
	*
	* This file is a part of CoreRenamer.
	* A batch file renamer for C Suite.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#pragma once

#include <QWidget>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>


class QUndoStack;
class settings;

QT_BEGIN_NAMESPACE
namespace Ui {
	class corerenamer;
}
QT_END_NAMESPACE

class corerenamer : public QWidget {
	Q_OBJECT

	enum PageAction {
		ManagingFiles,
		RenamingFiles
	};

public:
	explicit corerenamer(QWidget *parent = nullptr);
	~corerenamer();

	void addFiles(const QStringList &list);

private:
	Ui::corerenamer *ui;
	settings *smi;
	QUndoStack *uStack;
	QSortFilterProxyModel *customSortM;
	QStandardItemModel *mModel;
	QString workingFilesPath;

	PageAction currentState;
	QString commonPath;
	QMap<QString, bool> filesMap;

	int uiMode;
    bool windowMaximized;
    QSize listViewIconSize, toolsIconSize, windowSize;

	void loadSettings();
	void startSetup();
	void shotcuts();
	void setupIcons();

protected:
	void closeEvent(QCloseEvent *event) override;

private slots:
	void on_btnAddText_clicked();
	void on_txtAddText_textChanged(const QString &arg1);

	void on_btnRemoveText_clicked();

	void on_btnReplaceText_clicked();
	void on_txtReplaceText_textChanged(const QString &arg1);

	void on_btnNumFormatApply_clicked();
	void on_btnFormatLower_clicked();
	void on_btnFormatCapitalize_clicked();
	void on_btnFormatUpper_clicked();

	void on_applyRegex_clicked();
	void on_replaceRegexMatched_toggled(bool checked);

	void on_applyManualRename_clicked();

	void on_btnSaveChangesList_clicked();

	void on_rename_clicked();

	void on_bUndo_clicked();
	void on_bRedo_clicked();

	void on_bRemoveItems_clicked();
	void on_bClearItems_clicked();

	void on_bGotoRenaming_clicked();

private:
	void openFileDialog();
	void listItemSelectionChanged(const QModelIndex &current, const QModelIndex &);
	void updateUndo(bool canUndo);
	void updateRedo(bool canRedo);

	void clearInputTexts();

	void updateButtonStateTo(PageAction action);

	bool isChanged(QStandardItem *item);
	bool isListContainsChanges();
	bool canIgnoreChanges();
	void getListOfChanges(QList<QStandardItem *> &changedItems);

	QIcon fileIcon(const QString &filePath);
	QString capitalizeEachWord(const QString &str);
	QString zeroPadText(int number, int count);
};

class customSortProxyM : public QSortFilterProxyModel {
protected:
	bool lessThan(const QModelIndex &left, const QModelIndex &right) const;
};
