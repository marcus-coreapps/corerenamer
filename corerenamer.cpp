/*
	*
	* This file is a part of CoreRenamer.
	* A batch file renamer for C Suite.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#include <QFileDialog>
#include <QMimeDatabase>
#include <QFileInfo>
#include <QDir>
#include <QDebug>
#include <QCollator>
#include <QMessageBox>
#include <QShortcut>
#include <QCloseEvent>
#include <QScroller>
#include <QUndoStack>
#include <QRandomGenerator>

#include <cprime/messageengine.h>
#include <cprime/activitesmanage.h>
#include <cprime/themefunc.h>

#include "undocommands.h"
#include "corerenamer.h"
#include "ui_corerenamer.h"

#include "settings.h"


QMimeDatabase mimes;

corerenamer::corerenamer(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::corerenamer)
	, smi(new settings)
	, uStack(new QUndoStack(this))
	, customSortM(new customSortProxyM())
	, mModel(new QStandardItemModel(0, 3, this))
	, currentState(ManagingFiles)
	, uiMode(0)
	, listViewIconSize(32, 32)
    , toolsIconSize(24, 24)
{
	ui->setupUi(this);

	loadSettings();
	startSetup();
	shotcuts();
}

corerenamer::~corerenamer()
{
	delete mModel;
	delete customSortM;
	delete smi;
	delete ui;
}

void corerenamer::addFiles(const QStringList &list)
{
	if (!list.length()) {
		return;
	}

	bool firstTime = false;

	if (!commonPath.length()) {
		firstTime = true;

		mModel->clear();
		uStack->clear();

		mModel->setColumnCount(3);
		mModel->setRowCount(0);

		mModel->setHeaderData(0, Qt::Horizontal, QObject::tr("Selected"));
		mModel->setHeaderData(1, Qt::Horizontal, QObject::tr("Changed"));
		mModel->setHeaderData(2, Qt::Horizontal, QObject::tr("Type"));

		commonPath = QFileInfo(list.first()).absolutePath();
	} else {
		QStringList paths;

		Q_FOREACH (QString path, list) {
			paths << QFileInfo(path).absolutePath();
		}

		QString str = commonPath;

		if (commonPath.startsWith("~")) {
			str = QDir::homePath() + commonPath.mid(1);
		}

		paths << str;

		/* Get shortest path: Shortest path is the one with least number of '/' */
		QString shortest = paths.at(0);
		int count        = INT_MAX;

		Q_FOREACH (QString path, paths) {
			if (path.count("/") < count) {
				count    = path.count("/");
				shortest = path;
			}
		}

		commonPath = shortest;
	}

	if (commonPath.startsWith(QDir::homePath())) {
		commonPath = "~" + commonPath.mid(QDir::homePath().length());
	}

	QStringList invalidPaths;

	Q_FOREACH (auto str, list) {
		if (!str.length()) {
			continue;
		}

		QFileInfo fileInfo(str);

		if (!fileInfo.isWritable()) {
			invalidPaths.append(str);
			continue;
		}

		if (filesMap[str]) {
			continue;
		}

		QFileInfo info(str);

		QStandardItem *item1 = new QStandardItem(info.completeBaseName());
		item1->setIcon(fileIcon(str));
		item1->setData(info.absoluteFilePath(), Qt::UserRole);

		QStandardItem *item2 = new QStandardItem("");
		item2->setData(info.absoluteFilePath(), Qt::UserRole);

		mModel->appendRow({ item1, item2, new QStandardItem(info.suffix()) });

		filesMap[str] = true;
	}

	if (firstTime) {
		customSortM->setSourceModel(mModel);
		ui->FLists->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
		ui->FLists->setModel(customSortM);

		int s = ui->FLists->horizontalHeader()->size().width();
		int j = static_cast<int>(s * .1);
		ui->FLists->horizontalHeader()->setMinimumSectionSize(j);
		ui->FLists->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
		ui->FLists->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
		ui->FLists->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Fixed);

		ui->renameTools->setCurrentIndex(0);

		connect(ui->FLists->selectionModel(), &QItemSelectionModel::currentChanged, this, &corerenamer::listItemSelectionChanged);
	}

	ui->page->setCurrentIndex(1);

	setWindowTitle(commonPath + " - CoreRenamer");

	updateButtonStateTo(currentState);

	if (!invalidPaths.empty()) {
		QMessageBox::warning(this, tr("Warning"), tr("Some selected files are write protected, which can not be renamed."));
		qWarning() << "Can not rename";
		qWarning() << invalidPaths;
	}
}

void corerenamer::loadSettings()
{
    // get CSuite's settings
    listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");
    uiMode = smi->getValue("CoreApps", "UIMode");

    // get app's settings
    windowSize = smi->getValue("CoreRenamer", "WindowSize");
    windowMaximized = smi->getValue("CoreRenamer", "WindowMaximized");
}

void corerenamer::startSetup()
{
	QPalette pltt(palette());

	pltt.setColor(QPalette::Base, Qt::transparent);
	setPalette(pltt);

	ui->bUndo->setEnabled(false);
	ui->bRedo->setEnabled(false);

	ui->FLists->setColumnWidth(1, 500);
	ui->FLists->setIconSize(listViewIconSize);

    ui->page->setCurrentIndex(0);
    this->resize(800, 500);

    if (uiMode == 2) {
        // setup mobile UI
        this->setWindowState(Qt::WindowMaximized);

        QScroller::grabGesture(ui->FLists, QScroller::LeftMouseButtonGesture);

        setupIcons();
        // all toolbuttons icon size in toolBar
        QList<QToolButton *> toolBtns2 = ui->toolBar->findChildren<QToolButton *>();

        Q_FOREACH (QToolButton *b, toolBtns2) {
            if (b) {
                b->setIconSize(toolsIconSize);
                b->setToolButtonStyle(Qt::ToolButtonIconOnly);
            }
        }
    } else {
        // setup desktop or tablet UI

        if(windowMaximized){
            this->setWindowState(Qt::WindowMaximized);
            qDebug() << "window is maximized";
        } else{
            this->resize(windowSize);
        }
    }

	connect(ui->chkAddIndex, &QCheckBox::toggled, ui->txtAddIndex, &QSpinBox::setEnabled);
	connect(ui->chkRemoveUntilEnd, &QCheckBox::toggled, ui->txtRmToIndex, &QSpinBox::setDisabled);

	connect(ui->addFiles, &QPushButton::clicked, this, &corerenamer::openFileDialog);
	connect(ui->bAddItems, &QToolButton::clicked, this, &corerenamer::openFileDialog);

	connect(uStack, &QUndoStack::canUndoChanged, this, &corerenamer::updateUndo);
	connect(uStack, &QUndoStack::canRedoChanged, this, &corerenamer::updateRedo);
}

void corerenamer::shotcuts()
{
	QShortcut *shortcut;

	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_N), this);
	connect(shortcut, &QShortcut::activated, this, &corerenamer::openFileDialog);

	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_Z), this);
	connect(shortcut, &QShortcut::activated, this, &corerenamer::on_bUndo_clicked);

	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::ShiftModifier | Qt::Key_Z), this);
	connect(shortcut, &QShortcut::activated, this, &corerenamer::on_bRedo_clicked);
}

void corerenamer::setupIcons()
{
	ui->bGotoRenaming->setIcon(CPrime::ThemeFunc::themeIcon("media-playback-start-symbolic", "media-playback-start", "media-playback-start"));
	ui->bRemoveItems->setIcon(CPrime::ThemeFunc::themeIcon("edit-delete-symbolic", "edit-delete", "edit-delete"));
	ui->bAddItems->setIcon(CPrime::ThemeFunc::themeIcon("document-new-symbolic", "document-new", "document-new"));
	ui->bUndo->setIcon(CPrime::ThemeFunc::themeIcon("edit-undo-symbolic", "edit-undo", "edit-undo"));
	ui->bRedo->setIcon(CPrime::ThemeFunc::themeIcon("edit-redo-symbolic", "edit-redo", "edit-redo"));
	ui->rename->setIcon(CPrime::ThemeFunc::themeIcon("object-select-symbolic", "object-select", "object-select"));
}

void corerenamer::closeEvent(QCloseEvent *event)
{
    qDebug()<< "save window stats"<< this->size() << this->isMaximized();

    smi->setValue("CoreRenamer", "WindowSize", this->size());
    smi->setValue("CoreRenamer", "WindowMaximized", this->isMaximized());

	if (!canIgnoreChanges()) {
		event->ignore();
	}
}

void corerenamer::on_btnAddText_clicked()
{
	QString newText = ui->txtAddText->text();

	if (!newText.length()) {
		QMessageBox::warning(this, tr("Empty text"), tr("Enter some text to add."));
		return;
	}

	QModelIndexList items = ui->FLists->selectionModel()->selectedRows();

	if (!items.length()) {
		for (int i = 0; i < ui->FLists->model()->rowCount(); i++) {
			items.append(ui->FLists->model()->index(i, 0));
		}
	}

	QHash<QStandardItem *, QString> changes;

	QString errorText  = "";
	int errorItemCount = 0;

	Q_FOREACH (auto item, items) {
        //QString sText = mModel->item(item.row(), 0)->text();
		QString str   = mModel->item(item.row(), 1)->text();

        // if name is empty add the original file name
        // commented as it's preventing to remove orignal name completly and rename it with something else
        //if (!str.length()) {
        //	str = sText;
        //}

		int pos = str.length();

		if (ui->chkAddFromLeft->isChecked()) {
			pos = 0;
		}

		if (ui->chkAddIndex->isChecked()) {
			pos = ui->txtAddIndex->value();

			if (ui->chkAddFromRight->isChecked()) {
				pos = str.length() - pos;
			}
		}

		if ((pos > str.length()) || (pos < 0)) {
			errorText += QString::number(item.row() + 1) + ", ";
			errorItemCount++;
			continue;
		}

		QString change = str.insert(pos, newText);
		changes[mModel->item(item.row(), 1)] = change;
	}

	if (errorText.length()) {
		errorText = errorText.mid(0, errorText.length() - 1);
		errorText = tr("Index out of bound for ") + QString::number(errorItemCount) + tr("item(s).") + "\nThese are " + errorText;
		QMessageBox::warning(this, tr("Can not add text"), errorText);
	}

	if (changes.count()) {
		uStack->beginMacro(QString("%1: Append or insert text").arg(QRandomGenerator::system()->generate64()));

		Q_FOREACH (auto item, changes.keys()) {
			uStack->push(new ARRTextCommand(item, changes[item], ADD));
		}

		uStack->endMacro();
	}
}

void corerenamer::on_txtAddText_textChanged(const QString &arg1)
{
	ui->btnAddText->setEnabled(arg1.length());
}

void corerenamer::on_btnRemoveText_clicked()
{
	int from   = ui->txtRmFromIndex->value();
	int to     = ui->txtRmToIndex->value();
	bool toEnd = ui->chkRemoveUntilEnd->isChecked();

	if ((from < 0) || (!toEnd && (to < 0))) {
		QMessageBox::warning(this, tr("Invalid index"),
							 QString(tr("Index %1 out of bound.")).arg((from < 0) ? tr("from") : tr("to")));
		return;
	}

	if (!toEnd && (from >= to)) {
		QMessageBox::warning(this, tr("Same index"),
							 QString("%1").arg(
								 (from == to ? tr("Both from and to index are equal") : tr("From index is greater than to index."))
							 ));
		return;
	}

	QModelIndexList items = ui->FLists->selectionModel()->selectedRows();

	if (!items.length()) {
		for (int i = 0; i < ui->FLists->model()->rowCount(); i++) {
			items.append(ui->FLists->model()->index(i, 0));
		}
	}

	QHash<QStandardItem *, QString> changes;

	QString errorText  = "";
	int errorItemCount = 0;

	Q_FOREACH (auto item, items) {
		QString sText = mModel->item(item.row(), 0)->text();
		QString str   = mModel->item(item.row(), 1)->text();

		if (!str.length()) {
			str = sText;
		}

		int len = str.length();

		if (toEnd) {
			to = len;
		}

		if ((from >= len) || (to > len)) {
			errorText += QString::number(item.row()) + ", ";
			errorItemCount++;
			continue;
		}

		QString change = str.remove(from, to - from);
		changes[mModel->item(item.row(), 1)] = change;
	}

	if (errorText.length()) {
		errorText = errorText.mid(0, errorText.length() - 1);
		errorText = errorText = tr("Index out of bound for ") + QString::number(errorItemCount) + tr("item(s).") + "\nThese are " + errorText;
		QMessageBox::warning(this, tr("Can not add text"), errorText);
	}

	if (changes.count()) {
		uStack->beginMacro(QString("%1: Remove text").arg(QRandomGenerator::system()->generate64()));

		Q_FOREACH (auto item, changes.keys()) {
			uStack->push(new ARRTextCommand(item, changes[item], ADD));
		}

		uStack->endMacro();
	}
}

void corerenamer::on_btnReplaceText_clicked()
{
	QString before = ui->txtReplaceText->text();

	if (!before.length()) {
		QMessageBox::warning(this, tr("Empty text"), tr("Enter some text to replace with."));
		return;
	}

	QModelIndexList items = ui->FLists->selectionModel()->selectedRows();

	if (!items.length()) {
		for (int i = 0; i < ui->FLists->model()->rowCount(); i++) {
			items.append(ui->FLists->model()->index(i, 0));
		}
	}

	QHash<QStandardItem *, QString> changes;

	Q_FOREACH (auto item, items) {
		QString sText = mModel->item(item.row(), 0)->text();
		QString str   = mModel->item(item.row(), 1)->text();

		if (!str.length()) {
			str = sText;
		}

		auto caseSensitivity = Qt::CaseInsensitive;

		if (ui->chkReplaceCaseSencitive->isChecked()) {
			caseSensitivity = Qt::CaseSensitive;
		}

		QString change = QString(str).replace(before, ui->txtReplaceWith->text(), caseSensitivity);

		if (change == str) {
			qWarning() << "Nothing changed";// Different
			continue;
		}

		changes[mModel->item(item.row(), 1)] = change;
	}

	if (changes.count()) {
		uStack->beginMacro(QString("%1: Replace text").arg(QRandomGenerator::system()->generate64()));

		Q_FOREACH (auto itemWid, changes.keys()) {
			uStack->push(new ARRTextCommand(itemWid, changes[itemWid], REPLACE));
		}

		uStack->endMacro();
	} else {
		QMessageBox::information(this, "Nothing Changed", QString("Given replace text ('" + before + "') not found."));
	}
}

void corerenamer::on_txtReplaceText_textChanged(const QString &arg1)
{
	ui->btnReplaceText->setEnabled(arg1.length());
}

void corerenamer::on_btnNumFormatApply_clicked()
{
	QModelIndexList items = ui->FLists->selectionModel()->selectedRows();

	if (!items.length()) {
		for (int i = 0; i < ui->FLists->model()->rowCount(); i++) {
			items.append(ui->FLists->model()->index(i, 0));
		}
	}

	QHash<QStandardItem *, QString> changes;

	int count = items.length();

	int number = 0;

	Q_FOREACH (auto item, items) {
		QString sText = mModel->item(item.row(), 0)->text();
		QString str   = mModel->item(item.row(), 1)->text();

		if (!str.length()) {
			str = sText;
		}

		number++;

		QString numberTxt = zeroPadText(number, count) + QString::number(number);
        QString formatTxt = ui->leNumberFormat->text().replace("<number>", numberTxt).replace("<space>", " ");
//		QString formatTxt = ui->cmbNumberFormats->currentText().replace("<number>", numberTxt).replace("<space>", " ");

		QString change;

		if (ui->chkNumFormatRight->isChecked()) {
//			std::reverse(formatTxt.begin(), formatTxt.end());

			change = str + formatTxt;
		} else {
			change = formatTxt + str;
		}

		changes[mModel->item(item.row(), 1)] = change;
	}

	if (changes.count()) {
		uStack->beginMacro(QString("%1: Format text with numbering").arg(QRandomGenerator::system()->generate64()));

		Q_FOREACH (auto item, changes.keys()) {
			uStack->push(new ARRTextCommand(item, changes[item], NUMBER));
		}

		uStack->endMacro();
	}
}

void corerenamer::on_btnFormatLower_clicked()
{
	QModelIndexList items = ui->FLists->selectionModel()->selectedRows();

	if (!items.length()) {
		for (int i = 0; i < ui->FLists->model()->rowCount(); i++) {
			items.append(ui->FLists->model()->index(i, 0));
		}
	}

	QHash<QStandardItem *, QString> changes;

	Q_FOREACH (auto item, items) {
		QString sText = mModel->item(item.row(), 0)->text();
		QString str   = mModel->item(item.row(), 1)->text();

		if (!str.length()) {
			str = sText;
		}

		QString change = QString(str).toLower();

		if (change == str) {
			qDebug() << "Nothing changed";
			continue;
		}

		changes[mModel->item(item.row(), 1)] = change;
	}

	if (changes.count()) {
		uStack->beginMacro(QString("%1: Format text to lower").arg(QRandomGenerator::system()->generate64()));

		Q_FOREACH (auto item, changes.keys()) {
			uStack->push(new ARRTextCommand(item, changes[item], CASE));
		}

		uStack->endMacro();
	}
}

void corerenamer::on_btnFormatCapitalize_clicked()
{
	QModelIndexList items = ui->FLists->selectionModel()->selectedRows();

	if (!items.length()) {
		for (int i = 0; i < ui->FLists->model()->rowCount(); i++) {
			items.append(ui->FLists->model()->index(i, 0));
		}
	}

	QHash<QStandardItem *, QString> changes;

	Q_FOREACH (auto item, items) {
		QString sText = mModel->item(item.row(), 0)->text();
		QString str   = mModel->item(item.row(), 1)->text();

		if (!str.length()) {
			str = sText;
		}

		QString change = capitalizeEachWord(str);

		if (change == str) {
			qDebug() << "Nothing changed";
			continue;
		}

		changes[mModel->item(item.row(), 1)] = change;
	}

	if (changes.count()) {
		uStack->beginMacro(QString("%1: Format text to capitalize").arg(QRandomGenerator::system()->generate64()));

		Q_FOREACH (auto item, changes.keys()) {
			uStack->push(new ARRTextCommand(item, changes[item], CASE));
		}

		uStack->endMacro();
	}
}

void corerenamer::on_btnFormatUpper_clicked()
{
	QModelIndexList items = ui->FLists->selectionModel()->selectedRows();

	if (!items.length()) {
		for (int i = 0; i < ui->FLists->model()->rowCount(); i++) {
			items.append(ui->FLists->model()->index(i, 0));
		}
	}

	QHash<QStandardItem *, QString> changes;

	Q_FOREACH (auto item, items) {
		QString sText = mModel->item(item.row(), 0)->text();
		QString str   = mModel->item(item.row(), 1)->text();

		if (!str.length()) {
			str = sText;
		}

		QString change = QString(str).toUpper();

		if (change == str) {
			qDebug() << "Nothing changed";
			continue;
		}

		changes[mModel->item(item.row(), 1)] = change;
	}

	if (changes.count()) {
		uStack->beginMacro(QString("%1: Format text to upper").arg(QRandomGenerator::system()->generate64()));

		Q_FOREACH (auto item, changes.keys()) {
			uStack->push(new ARRTextCommand(item, changes[item], CASE));
		}

		uStack->endMacro();
	}
}

void corerenamer::on_applyRegex_clicked()
{
	ui->regexError->clear();

	if (!ui->regexText->text().length()) {
		QMessageBox::warning(this, tr("Empty text"), tr("Enter regular expression text to search."));
		return;
	}

	if (ui->replaceRegexMatched->isChecked() && !ui->replaceTextRegex->text().length()) {
		QMessageBox::warning(this, tr("Empty text"), tr("Enter some text to replace with."));
		return;
	}

	QString regexStr = /*QRegularExpression::escape*/ (ui->regexText->text());
	// Place an escape character before any escapable character
	QString temp = "";
	QList<QChar> escapable = {'\\', '$', '?', '^', '*', '(', ')', '+', '[', ']', '|', '.'};
	Q_FOREACH (auto c, regexStr) {
		if (escapable.contains(c)) {
			temp += '\\';
		}
		temp += c;
	}
	regexStr = temp;

	QRegularExpression regex(regexStr);

	if (!regex.isValid()) {
		QString errorStr = regex.errorString();
		int errorOffset  = regex.patternErrorOffset();

		ui->regexError->setText(QString("%1 offset %2").arg(errorStr).arg(errorOffset));
		QMessageBox::warning(this, tr("Regular Expression Error"), QString("%1 (Offset %2)").arg(tr(errorStr.toStdString().c_str())).arg(errorOffset));
		return;
	}

	QModelIndexList items = ui->FLists->selectionModel()->selectedRows();

	if (!items.length()) {
		for (int i = 0; i < ui->FLists->model()->rowCount(); i++) {
			items.append(ui->FLists->model()->index(i, 0));
		}
	}

	QHash<QStandardItem *, QString> changes;

	Q_FOREACH (auto item, items) {
		QString sText = mModel->item(item.row(), 0)->text();
		QString str   = mModel->item(item.row(), 1)->text();

		if (!str.length()) {
			str = sText;
		}

		QString change;

		if (ui->deleteRegexMatched->isChecked()) {
			change = QString(str).remove(regex);
		} else {
			change = QString(str).replace(regex, ui->replaceTextRegex->text());
		}

		if (change == str) {
			continue;
		}

		changes[mModel->item(item.row(), 1)] = change;
	}

	if (changes.count()) {
		uStack->beginMacro(QString("%1: Regex text").arg(QRandomGenerator::system()->generate64()));

		Q_FOREACH (auto itemWid, changes.keys()) {
			uStack->push(new ARRTextCommand(itemWid, changes[itemWid], REPLACE));
		}

		uStack->endMacro();
	} else {
		QMessageBox::information(this, "Nothing Changed", QString("Given regex ('" + regexStr + "') does not match to any item."));
	}
}

void corerenamer::on_replaceRegexMatched_toggled(bool checked)
{
	ui->replaceTextRegex->setEnabled(checked);
}

void corerenamer::on_applyManualRename_clicked()
{
	QModelIndexList items = ui->FLists->selectionModel()->selectedRows();

	if (items.length() > 1) {
		QMessageBox::warning(this, "Can not rename", "Manual rename only works for a single file.\nSelect one file to change the name.");
	} else if (items.length() == 0) {
		QMessageBox::warning(this, "Empty selection", "No file is selected. Please select one file to rename.");
	} else {
		if (!ui->manualNewName->text().length()) {
			QMessageBox::warning(this, "Empty text", "Enter new name for the selected file.");
			return;
		}

		auto index = items[0];

		uStack->beginMacro(QString("%1: Manual rename text").arg(QRandomGenerator::system()->generate64()));
		uStack->push(new ARRTextCommand(mModel->item(index.row(), 1), ui->manualNewName->text(), REPLACE));
		uStack->endMacro();
	}
}

void corerenamer::on_btnSaveChangesList_clicked()
{
	QList<QStandardItem *> changedItems;

	getListOfChanges(changedItems);

	if (!changedItems.length()) {
		QMessageBox::warning(this, tr("No changes"), tr("Nothing to save as no changes occurred."));
		return;
	}

	ui->lblSavedChangesListAt->clear();

	QString saveFilePath = QFileDialog::getSaveFileName(this, tr("Save file"), workingFilesPath);

	if (!saveFilePath.length()) {
		return;
	}

	if (!QFileInfo(saveFilePath).absoluteDir().exists()) {
		QMessageBox::warning(this, tr("Invalid file path"), tr("File path does not exists."));
		return;
	}

	QString txt = "";

	Q_FOREACH (auto item, changedItems) {
		QString type = mModel->item(item->row(), 2)->text();

		type = (type.length() ? "." + type : "");

		QString filepath        = mModel->item(item->row(), 0)->data(Qt::UserRole).toString();
		QString currentFileName = mModel->item(item->row(), 0)->text();
		QString changedFileName = item->text();

		QFileInfo fileInfo(filepath);

		if (ui->chkOnlyFileName->isChecked()) {
			txt += currentFileName + type + " -> " + changedFileName + type;
		} else {
			txt += fileInfo.absolutePath() + "/" + currentFileName + type + " -> " +
				   fileInfo.absolutePath() + "/" + changedFileName + type;
		}

		txt += "\n";
	}

	QFile file(saveFilePath);

	if (file.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text)) {
		file.write(txt.toLatin1());
		file.close();
		ui->lblSavedChangesListAt->setText(tr("File saved at: ") + saveFilePath);
		CPrime::MessageEngine::showMessage("org.cubocore.CoreRenamer", "CoreRenamer",
											 tr("File change list saved successfully"), tr("List saved at ") + saveFilePath);
	} else {
		ui->lblSavedChangesListAt->setText(tr("Can not create file at: ") + saveFilePath);
		CPrime::MessageEngine::showMessage("org.cubocore.CoreRenamer", "CoreRenamer",
											 tr("File can not be created"), tr("Location '") + saveFilePath + tr("' is not writtable."));
	}
}

void corerenamer::on_rename_clicked()
{
	QList<QStandardItem *> changedItems;

	getListOfChanges(changedItems);

	if (!changedItems.length()) {
		QMessageBox::warning(this, tr("No changes"), tr("Nothing to rename."));
		return;
	}

	auto reply = QMessageBox::question(this, tr("Warning"),
									   tr("Are you sure to rename those files?\nIf you once rename them it is not possible to go back."));

	if (reply == QMessageBox::No) {
		return;
	}

	QVector<QPair<QString, QString> > fileList;

	Q_FOREACH (auto item, changedItems) {
		QString type = mModel->item(item->row(), 2)->text();
		type = (type.length() ? "." + type : "");

		QString filepath        = item->data(Qt::UserRole).toString();
		QString currentFileName = mModel->item(item->row(), 0)->text();
		QString changedFileName = item->text();

		QFileInfo fileInfo(filepath);

		QString oldFilepath = fileInfo.absolutePath() + "/" + currentFileName + type;
		QString newFilepath = fileInfo.absolutePath() + "/" + changedFileName + type;

		if (QFile(newFilepath).exists()) {
			QMessageBox::warning(this, tr("Already exists"),
								 tr("New named file ") + changedFileName + tr(" for ") + "\n\"" + oldFilepath + "\"\n" + tr(" already exist"));
			return;
		}

		fileList.push_back({ oldFilepath, newFilepath });
	}

	QStringList newFileList;
	QStringList failedList;

	Q_FOREACH (auto p, fileList) {
		bool done = QFile::rename(p.first, p.second);
		qDebug() << p.first << "Renamed to" << p.second << "..." << done;

		if (done) {
			newFileList.append(p.second);
		} else {
			newFileList.append(p.first);
			failedList.append(p.first);
		}
	}

	if (failedList.length()) {
		QMessageBox::warning(this, tr("File not renamed"), tr("Some files can not be renamed."));
		qWarning() << "Some files could not be renamed...";
		qWarning() << failedList;
	} else {
		CPrime::MessageEngine::showMessage("org.cubocore.CoreRenamer", "CoreRenamer", tr("File Renamed Successfully"), tr("All tasks are done."));
	}

	currentState = ManagingFiles;
	commonPath   = QString();
	mModel->clear();

	clearInputTexts();
	addFiles(newFileList);
}

void corerenamer::on_bUndo_clicked()
{
	uStack->undo();
}

void corerenamer::on_bRedo_clicked()
{
	uStack->redo();
}

void corerenamer::on_bRemoveItems_clicked()
{
	QModelIndexList items = ui->FLists->selectionModel()->selectedRows();

	if (items.length() && (items.length() < mModel->rowCount())) {
		Q_FOREACH (const QModelIndex &i, items) {
			filesMap.remove(mModel->item(i.row())->data(Qt::UserRole).toString());
			mModel->removeRow(i.row());
		}
	} else {
		ui->FLists->model()->removeRows(0, ui->FLists->model()->rowCount());
		filesMap.clear();
	}
}

void corerenamer::on_bClearItems_clicked()
{
	if (canIgnoreChanges()) {
		currentState = ManagingFiles;
		commonPath   = QString();
		ui->FLists->model()->removeRows(0, ui->FLists->model()->rowCount());

		clearInputTexts();

		ui->renameTools->setCurrentIndex(0);
		ui->page->setCurrentIndex(1);

		setWindowTitle(" - CoreRenamer");
		updateButtonStateTo(currentState);
	}
}

void corerenamer::on_bGotoRenaming_clicked()
{
	if (!ui->FLists->model()->rowCount()) {
		QMessageBox::information(this, tr("Empty items for renaming"), tr("Please add some files to start renaming them."));
		return;
	}

	if (currentState == ManagingFiles) {
		currentState = RenamingFiles;
		updateButtonStateTo(currentState);
	}
}

void corerenamer::openFileDialog()
{
	QString openFrom = workingFilesPath;

	if (workingFilesPath.length() == 0) {
		openFrom = QDir::homePath();
	}

	QStringList list = QFileDialog::getOpenFileNames(this, tr("Add files"), openFrom, "*");

	if (list.length() > 0) {
		workingFilesPath = QFileInfo(list[0]).path();
		addFiles(list);
	}
}

void corerenamer::listItemSelectionChanged(const QModelIndex &current, const QModelIndex &)
{
	auto item = mModel->item(current.row());

	if (!item) {
		return;
	}

	QString sText = mModel->item(item->row(), 0)->text();
	QString str   = mModel->item(item->row(), 1)->text();

	if (!str.length()) {
		str = sText;
	}

	ui->txtReplaceText->setText(str);
	ui->manualSelectedFile->setText("Selected file: " + str);
}

void corerenamer::updateUndo(bool canUndo)
{
	ui->bUndo->setEnabled(canUndo);
}

void corerenamer::updateRedo(bool canRedo)
{
	ui->bRedo->setEnabled(canRedo);
}

void corerenamer::clearInputTexts()
{
	uStack->clear();
	filesMap.clear();

	ui->txtAddText->clear();
	ui->txtAddIndex->clear();
	ui->txtRmFromIndex->clear();
	ui->txtRmToIndex->clear();
	ui->txtReplaceText->clear();
	ui->txtReplaceWith->clear();
	ui->lblSavedChangesListAt->clear();
	ui->manualNewName->clear();
	ui->manualSelectedFile->setText("Selected file:");

	ui->chkAddIndex->setChecked(false);
	ui->chkRemoveUntilEnd->setChecked(false);
	ui->chkNumFormatRight->setChecked(false);
	ui->chkReplaceCaseSencitive->setChecked(false);
	ui->chkOnlyFileName->setChecked(true);
}

void corerenamer::updateButtonStateTo(PageAction action)
{
	ui->bAddItems->setVisible(action == ManagingFiles);
	ui->bRemoveItems->setVisible(action == ManagingFiles);
	ui->bGotoRenaming->setVisible(action == ManagingFiles);
	ui->bUndo->setVisible(action == RenamingFiles);
	ui->bRedo->setVisible(action == RenamingFiles);
	ui->rename->setVisible(action == RenamingFiles);
	ui->renameTools->setVisible(action == RenamingFiles);
	ui->bClearItems->setVisible(action == RenamingFiles);

	ui->renameTools->setCurrentIndex(0);

	ui->bUndo->setDisabled(true);
	ui->bRedo->setDisabled(true);
}

bool corerenamer::isChanged(QStandardItem *item)
{
	if (item->text().length()) {
		QString filepath = item->data(Qt::UserRole).toString();
		QFileInfo info(filepath);

		if (info.completeBaseName() != item->text()) {
			return true;
		}
	}

	return false;
}

bool corerenamer::isListContainsChanges()
{
	if (!ui->FLists->model()) {
		return false;
	}

	int count = ui->FLists->model()->rowCount();

	for (int i = 0; i < count; i++) {
		if (isChanged(mModel->item(i, 1))) {
			return true;
		}
	}

	return false;
}

bool corerenamer::canIgnoreChanges()
{
	if (isListContainsChanges()) {
		auto reply = QMessageBox::question(this, tr("Ignore changes"),
										   tr("Do you want to ignore file name changes?"),
										   QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

		if (reply == QMessageBox::No) {
			return false;
		}
	}

	return true;
}

void corerenamer::getListOfChanges(QList<QStandardItem *> &changedItems)
{
	if (!ui->FLists->model()) {
		return;
	}

	int count = ui->FLists->model()->rowCount();

	for (int i = 0; i < count; i++) {
		auto item = mModel->item(i, 1); // Changed Item

		if (isChanged(item)) {
			changedItems.append(item);
		}
	}
}

QIcon corerenamer::fileIcon(const QString &filePath)
{
	QMimeType mime = mimes.mimeTypeForFile(filePath);

	return QIcon::fromTheme(mime.iconName(),
							QIcon::fromTheme(mime.genericIconName(),
									QApplication::style()->standardIcon(QStyle::SP_FileIcon)));
}

QString corerenamer::capitalizeEachWord(const QString &str)
{
	bool doChange   = true;
	QString changed = str;
	auto it         = changed.begin();

	while (it != changed.end()) {
		if (it->isDigit() || it->isPunct()) {
			it++;
			continue;
		}

		if (doChange) {
			*it      = (*it).toUpper();
			doChange = false;
		} else if ((*it).isSpace()) {
			doChange = true;
		} else {
			*it = (*it).toLower();
		}

		it++;
	}

	return changed;
}

QString corerenamer::zeroPadText(int number, int count)
{
	QString pad  = "";
	int padCount = QString::number(count).length() - QString::number(number).length();

	while (padCount--) {
		pad += "0";
	}

	return pad;
}

bool customSortProxyM::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
	QVariant leftData  = sourceModel()->data(left);
	QVariant rightData = sourceModel()->data(right);

	QCollator qCollator;

	qCollator.setNumericMode(true);
	return (qCollator.compare(leftData.toString(), rightData.toString()) < 0);
}
